/*
 * board_stm32e407.c
 *
 *  Created on: 21.10.2018
 *      Author: adib
 */

#include "stm32f4xx_hal.h"
#include "main.h"

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim5;

static unsigned int min(unsigned int a, unsigned int b)
{
   if (a < b) {
      return a;
   }
   return b;
}

unsigned int tick_usec(void)
{
	return __HAL_TIM_GET_COUNTER(&htim2);
}

unsigned int tick_milli(void)
{
	return __HAL_TIM_GET_COUNTER(&htim5);
}

int tick_diff(unsigned int start, unsigned int stop)
{
	// 32 bit correct difference based on unsigned math
	uint32_t diff = stop-start;
	return (int32_t)diff;
}

void delay_usec(unsigned int wait)
{
	while(wait > 0) {
		unsigned int delay = min(INT32_MAX, wait);
		unsigned int start = tick_usec();
		while(tick_diff(start, tick_usec()) <= delay) {
		}
		wait -= delay;
	}
}

void delay_milli(unsigned int wait)
{
	while(wait > 0) {
		unsigned int delay = min(INT32_MAX, wait);
		unsigned int start = tick_milli();
		while(tick_diff(start, tick_milli()) <= delay) {
		}
		wait -= delay;
	}
}
