/**
	@file at86rf212.c

 	@author theadib@gmail.com
	@license: SPDX-License-Identifier: MIT+

	initial byte
	10xx xxxx register read
	11xx xxxx register write
	001r rrrr frame buffer read
	011r rrrr frame buffer write
	000r rrrr sram read
	010r rrrr sram write

*/

#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_gpio.h"
#include "main.h"
#include "board_stm32e407.h"

extern SPI_HandleTypeDef hspi2;

#define REGISTER_PART_NUM 0x1c
#define REGISTER_VERSION_NUM 0x1d
#define REGISTER_MAN_ID_0 0x1e
#define REGISTER_MAN_ID_1 0x1f

typedef struct {

} at86rf212_ctx_t;

static void at86rf212_rst_high(void)
{
	LL_GPIO_SetOutputPin(UEXT3_GPIO_Port, UEXT3_Pin);
}

static void at86rf212_rst_low(void)
{
	LL_GPIO_ResetOutputPin(UEXT3_GPIO_Port, UEXT3_Pin);
}

static void at86rf212_slptr_high(void)
{
	LL_GPIO_SetOutputPin(UEXT5_GPIO_Port, UEXT5_Pin);
}

static void at86rf212_slptr_low(void)
{
	LL_GPIO_ResetOutputPin(UEXT5_GPIO_Port, UEXT5_Pin);
}

static void at86rf212_cs_high(void)
{
	LL_GPIO_SetOutputPin(UEXT_CS_GPIO_Port, UEXT_CS_Pin);
}

static void at86rf212_cs_low(void)
{
	LL_GPIO_ResetOutputPin(UEXT_CS_GPIO_Port, UEXT_CS_Pin);
}

static unsigned int at86rf212_register_read(unsigned int addr)
{
	at86rf212_cs_low();
	uint8_t cmd[2] = { 0x80 | (addr & 0x3f), 0 };
	__DSB();
	HAL_SPI_TransmitReceive(&hspi2, cmd, cmd, 2, 10);
	at86rf212_cs_high();

	return cmd[1];
}

static void at86rf212_register_write(unsigned int addr, uint8_t val)
{
	at86rf212_cs_low();
	uint8_t cmd[2] = { 0xc0 | (addr & 0x3f), val };
	__DSB();
	HAL_SPI_Transmit(&hspi2, cmd, 2, 10);
	at86rf212_cs_high();

	return cmd[1];
}

void at86rf212_init(void)
{
	at86rf212_rst_low();
	uint8_t dummy = 0;
	HAL_SPI_Transmit(&hspi2, &dummy, 1, 100);
	delay_milli(2);
	at86rf212_slptr_low();
	at86rf212_rst_high();
	delay_milli(2);

}


unsigned int at86rf212_devid(void)
{
	unsigned int part_num = at86rf212_register_read(REGISTER_PART_NUM);
	unsigned int version_num = at86rf212_register_read(REGISTER_VERSION_NUM);
	unsigned int man_id0 = at86rf212_register_read(REGISTER_MAN_ID_0);
	unsigned int man_id1 = at86rf212_register_read(REGISTER_MAN_ID_1);

	return (man_id1 << 24) | (man_id0 << 16) | (version_num << 8) | part_num;
}

void at86rf212_configure(void)
{

}

/// reset the chip, similar to init() but do not reset HAL
void at86rf212_reset(void)
{
	at86rf212_rst_low();
	delay_milli(2);
	at86rf212_slptr_low();
	at86rf212_rst_high();
	delay_milli(2);
}

///emit continuous wave signal
void at86rf212_contwave(void)
{
	at86rf212_register_write(0x0e, 0x01);
	at86rf212_register_write(0x02, 0x03);
	// set channel
	// cc_band = 5, cc_number = 35
	at86rf212_register_write(0x13, 0x23);
	at86rf212_register_write(0x13, 0x05);
	// set tx power
	// GC_TX_OFFS should be set to 3
	at86rf212_register_write(0x05, 0xad);	// 0db
	at86rf212_register_write(0x16, 0x03);	// GC_TX_OFFS
	// verify TRX_OFF 0x08
	delay_milli(3);
	at86rf212_register_read(0x01);

	at86rf212_register_write(0x36, 0x0f);
	at86rf212_register_write(0x1c, 0x54);
	at86rf212_register_write(0x1c, 0x42);
	at86rf212_register_write(0x34, 0x00);
	at86rf212_register_write(0x3f, 0x08);
	at86rf212_register_write(0x02, 0x09);	// enable PLL_ON
	// wait for PLL_LOCK 0x01
	delay_milli(3);
	at86rf212_register_read(0x0f);
	// initiate transmission
	at86rf212_register_write(0x02, 0x02);

}

