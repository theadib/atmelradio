/*
    @file at86rf212.h
    @author theadib@gmail.com
    @license: SPDX-License-Identifier: MIT+
*/

#ifndef INC_AT86RF212_H_
#define INC_AT86RF212_H_


extern void at86rf212_init(void);
extern unsigned int at86rf212_devid(void);
extern void at86rf212_configure(void);
extern void at86rf212_reset(void);
extern void at86rf212_contwave(void);




#endif /* INC_AT86RF212_H_ */
