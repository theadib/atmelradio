/*
 * board_stm32e407.h
 *
 *  Created on: 21.10.2018
 *      Author: adib
 */

#ifndef INC_BOARD_STM32E407_H_
#define INC_BOARD_STM32E407_H_

extern unsigned int tick_usec(void);
extern unsigned int tick_milli(void);

extern int tick_diff(unsigned int start, unsigned int stop);

extern void delay_usec(unsigned int);
extern void delay_milli(unsigned int);

#endif /* INC_BOARD_STM32E407_H_ */
