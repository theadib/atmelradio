
== stm32-e407 test

test application to test basic functionality transmit and receive

based on Olimex stm32-e407 board and RZ600 module on UEXT connector.

SPI mode 0: CLK Phase:first, CLK polarity:low


UEXT    rf212
1-3.3V  10        black
2-GND    9        white
3-PC6    1 /RST   brown
4-PC7    3 IRQ    orange
5-PB8    4 SLP_TR yellow
6-PB9    2 MISC   red can be jumpered to CLK or TST
7-MISO   7        lila
8-MOSI   6        blue
9-SCK    8        grey
10-CS    5        green

=== operation

after reset the applicaion is in RX mode
on pressing the button a sequence of XXX (1.000.000) protocols are emitted
- the length of those protocols is yyy (29 as in RTK3 - 23+2+2+2) bytes
- the content is a 32 counter and remaining bytes are preset random
- the given hardware CRC is doublechecked
the receiver documents the last received packet number
